﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace WindowsFormsApplication3
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        /// <summary>
        /// Создать приложение, копирующее заданный пользователем
        /// файл в указанную им папку.При этом пользователь должен
        /// иметь переименовать файл.Копировать в несуществующую
        /// папку нельзя.
        /// </summary>


        public void ChooseFolder1()
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                textBox1.Text = openFileDialog1.FileName;
            }
        }

        public void ChooseFolder2()
        {
            if (folderBrowserDialog2.ShowDialog() == DialogResult.OK)
            {
                textBox2.Text = folderBrowserDialog2.SelectedPath;
            }
        }

        public void FindDirectory(string locationDir)
        {
            bool exist = Directory.Exists(locationDir);
            if (!exist)
            {
                DialogResult res = MessageBox.Show("Такой папки нет! \n Создать новую?", "Ошибка!", MessageBoxButtons.YesNo);
                if (res == DialogResult.Yes)
                {
                    DirectoryInfo di = Directory.CreateDirectory(locationDir);
                }
            }
        }
        public void CopyTo(string selectFile, string locationFile)
        {
            FindDirectory(locationFile);
            //нужно ли новое имя файлу?
            string startName = Path.GetFileName(selectFile);
            string old_exe = Path.GetExtension(selectFile);
            string newName = NewName.newName;
            string endName;
            if (newName == null)
            {
                endName = startName;
            }
            else
            {
                endName = newName + old_exe;
            }
            try
            {
                File.Copy(selectFile, locationFile + "/" + endName);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
            
        }
        
        private void button1_Click(object sender, EventArgs e)
        {
            ChooseFolder1();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            ChooseFolder2();
        }
        private void button3_Click(object sender, EventArgs e)
        {
            CopyTo(textBox1.Text, textBox2.Text);
        }
        private void button4_Click_1(object sender, EventArgs e)
        {
            Form2 f2 = new Form2();
            f2.ShowDialog();
        }
        private void Form1_Load(object sender, EventArgs e)
        {
           
        }

        private void button4_Click(object sender, EventArgs e)
        {

        }

        
    }
}
